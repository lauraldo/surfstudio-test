package alpha.niolas.surfstudiotestapp.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import alpha.niolas.surfstudiotestapp.R;

/**
 * Created by lauraldo on 02.01.17.
 */

public class AlphaNumericAdapter extends RecyclerView.Adapter<AlphaNumericAdapter.ViewHolder> {

    private List<String> data;

    public AlphaNumericAdapter(List<String> data) {
        this.data = data;
    }

    public void refreshData(List<String> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.v_list_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ((TextView) holder.itemView).setText(data.get(position));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
