package alpha.niolas.surfstudiotestapp.ui;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import alpha.niolas.surfstudiotestapp.ui.adapter.AlphaNumericAdapter;
import alpha.niolas.surfstudiotestapp.R;
import alpha.niolas.surfstudiotestapp.databinding.ActivityMainBinding;
import ru.niolas.NotANaturalNumberException;
import ru.niolas.NumericConverter;
import ru.niolas.RangeViolationException;

public class MainActivity extends AppCompatActivity {

    private int viewMode;
    private ActivityMainBinding binding;
    private List<String> alphas, numerics;

    private LinearLayoutManager layoutManager = new LinearLayoutManager(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        loadItemsFromFile();
        viewMode = ViewMode.NUMERIC;
        binding.list.setLayoutManager(layoutManager);
        binding.list.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        binding.list.setAdapter(new AlphaNumericAdapter(numerics));
    }

    private void loadItemsFromFile() {
        try {

            alphas = new ArrayList<>();
            numerics = new ArrayList<>();

            InputStream fis = getAssets().open("input.txt");
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(fis));

            String line;
            while ((line = bufferedReader.readLine()) != null) {
                try {
                    numerics.add(line);
                    alphas.add(NumericConverter.toAlpha(Long.parseLong(line)));
                } catch (NumberFormatException | NotANaturalNumberException | RangeViolationException e) {
                    numerics.remove(numerics.size() - 1);
                }
            }

            bufferedReader.close();

        } catch (IOException e) {
            Toast.makeText(this, "Unable to read from file input.txt", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.switchMode) {
            switchMode();
        }
        return super.onOptionsItemSelected(item);
    }

    private void switchMode() {
        if (viewMode == ViewMode.ALPHA) {
            viewMode = ViewMode.NUMERIC;
            ((AlphaNumericAdapter) binding.list.getAdapter()).refreshData(numerics);
        } else {
            viewMode = ViewMode.ALPHA;
            ((AlphaNumericAdapter) binding.list.getAdapter()).refreshData(alphas);
        }
    }
}
