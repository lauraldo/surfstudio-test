package alpha.niolas.surfstudiotestapp.ui;

import android.support.annotation.IntDef;

/**
 * Created by lauraldo on 02.01.17.
 */

@IntDef({ViewMode.ALPHA,
        ViewMode.NUMERIC})
public @interface ViewMode {
    int ALPHA = 0;
    int NUMERIC = 1;
}