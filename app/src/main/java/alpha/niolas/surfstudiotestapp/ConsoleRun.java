package alpha.niolas.surfstudiotestapp;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import ru.niolas.NotANaturalNumberException;
import ru.niolas.NumericConverter;
import ru.niolas.RangeViolationException;

/**
 * Created by lauraldo on 02.01.17.
 */

public class ConsoleRun {

    public static void main(String[] args) {

        try {
            File dir = new File(".");
            File fin = new File(dir.getCanonicalPath() + File.separator + "input.txt");
            File fout = new File(dir.getCanonicalPath() + File.separator + "output.txt");

            FileInputStream fis = new FileInputStream(fin);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(fis));

            FileOutputStream fos = new FileOutputStream(fout);
            BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(fos));

            String line;
            while ((line = bufferedReader.readLine()) != null) {
                try {
                    bufferedWriter.write(NumericConverter.toAlpha(Long.parseLong(line)) + "\n");
                } catch (NumberFormatException | NotANaturalNumberException e) {
                    bufferedWriter.write("<Not a natural number>\n");
                } catch (RangeViolationException e) {
                    bufferedWriter.write("<Accepted range is [1; 999 999 999 999 999]>\n");
                }
            }

            bufferedReader.close();
            bufferedWriter.close();

        } catch (IOException e) {
            System.out.println("Unable to read from file input.txt or write to output.txt");
        }

    }

}
